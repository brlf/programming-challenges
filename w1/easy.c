#include <stdio.h>

int main(int argc, char **argv)
{
	char map[256] = { 0 };
	map[')'] = '(';
	map['}'] = '{';
	map[']'] = '[';

	while (--argc) {
		char *top, *p, c;
		top = p = *++argv;

		while ((c = *p++)) {
			if (top != *argv && map[(size_t)c] == top[-1]) {
				top--;
			} else {
				*top++ = c;
			}
		}

		*top = 0;

		puts(*argv);
	}
}
