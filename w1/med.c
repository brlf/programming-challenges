#include <stdio.h>
#include <stdlib.h>

char const brackets[6] = "({[)}]";

int update(int *buf, int len)
{
	int width = *buf >> 2;
	if (width > len - 2) {
		return -1;
	}
	if ((++*buf & 3) < 3) {
		return 0;
	}
	*buf &= ~3;

	if (update(buf + 1, width)) {
		buf[1] = 0;
		if (update(buf + width + 2, len - width - 2)) {
			*buf = (width += 2) << 2;
			if (width > len - 2) {
				return -1;
			}
			buf[width + 2] = 0;
		}
	}
	return 0;
}

void display(int *buf, int len)
{
	int width = *buf >> 2;
	char const *bracket = brackets + (*buf & 3);
	if (len <= 0) {
		return;
	};

	putchar(bracket[0]);
	display(buf + 1, width);
	putchar(bracket[3]);
	display(buf + width + 2, len - width - 2);
}

int main(int argc, char **argv)
{
	int n;
	int *buf;
	if (argc < 2) {
		fputs("Bad invocation", stderr);
		exit(EXIT_FAILURE);
	}

	sscanf(argv[1], "%d", &n);

	buf = calloc(n + 2, sizeof(*buf));

	display(buf, n);
	putchar('\n');
	while (!update(buf, n)) {
		display(buf, n);
		putchar('\n');
	}

	free(buf);
	return 0;
}
