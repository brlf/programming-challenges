#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

size_t fib(size_t n)
{
	/* n=93 is the last fib(n) that fits in u64. */
	static size_t cache[94] = { 0, 1 };
	static size_t top_valid = 1;
	size_t a, b;

	if (top_valid >= n) {
		return cache[n];
	} else if (n >= sizeof(cache) / sizeof(*cache)) {
		fputs("Fib overflow\n", stderr);
		exit(EXIT_FAILURE);
	}

	a = cache[top_valid - 1];
	b = cache[top_valid];
	for (; top_valid < n; top_valid += 2) {
		a = cache[top_valid + 1] = a + b;
		b = cache[top_valid + 2] = a + b;
	}

	return cache[n];
}

int main(int argc, char **argv)
{
	char *p, c;
	size_t combos = 1;
	int digits = 0;
	

	if (argc != 2) {
		fputs("Bad invocation.\n", stderr);
	}

	if (!*(p = argv[1])) {
		goto done;
	}

	for (;;) {
		c = *++p;
		if (c < '0' || c > '9') {
			combos *= fib(digits);
			digits = 0;
			if (!c) {
				break;
			}
		} else {
			digits++;
		}
	}

done:
	printf("%zu\n", combos);

	return 0;
}
