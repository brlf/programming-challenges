#include <stdio.h>
#include <stdlib.h>

void bail(char const *msg)
{
	fputs(msg, stderr);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	int count = 0;
	char *p, c = 0;

	if (argc != 2) {
		bail("Improper invocation.\n");
	}

	for (c = *(p = argv[1]); *p; p++) {
		if (c == *p) {
			count++;
		} else {
			printf("%c%d", c, count);
			c = *p;
			count = 1;
		}
	}

	if (count) {
		printf("%c%d", c, count);
	}

	putchar('\n');

	return 0;
}
